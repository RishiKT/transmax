﻿using System;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using IO;
using GradeApp;
using System.Collections.Generic;

namespace TestGradeApp
{
    [TestFixture]
    public class SortingTest
    {
        SortByGradeThenNameStrategy _sortStrategy;

        [OneTimeSetUp]
        public void Setup()
        {
            _sortStrategy = new SortByGradeThenNameStrategy();
        }

        /// <summary>
        /// Verify the SortContext interface is created
        /// </summary>
        [Test]
        public void ShouldCreateSortContext()
        {
            Action result = () => new SortContext(new Mock<ISortStrategy>().Object);
            result.ShouldNotThrow<ArgumentException>();
        }

        /// <summary>
        /// Verify exception is thrown on null argument to SortData
        /// </summary>
        [Test]
        public void ThrowExceptionOnNullData()
        {
            Action result = () => _sortStrategy.SortData(null);

            result.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("Sort data missing.");
        }

        /// <summary>
        /// Verify exception is thrown on empty argument to SortData
        /// </summary>
        [Test]
        public void ThrowExceptionOnNoData()
        {
            Action result = () => _sortStrategy.SortData(new List<IEmployeeResult>());

            result.ShouldThrow<ArgumentException>()
                .And.Message.Should().Contain("Sort data missing.");
        }

        /// <summary>
        /// Verify input data is sorted correctly according to score
        /// </summary>
        [Test]
        public void ShouldSortDataAccordingToScore()
        {
            var list = new List<IEmployeeResult>();
            var employee1 = new Employee
            {
                LastName = "Park",
                FirstName = "Deer",
                Score = 10,
            };
            var employee2 = new Employee
            {
                LastName = "Bon",
                FirstName = "Echo",
                Score = 20,
            };
            var employee3 = new Employee
            {
                LastName = "Paradiso",
                FirstName = "Gran",
                Score = 30,
            };

            list.Add(employee1);
            list.Add(employee2);
            list.Add(employee3);

            var result = _sortStrategy.SortData(list);

            result.Should().NotBeNullOrEmpty();

            //Verify results against expected
            result[0].Should().Be(employee3);
            result[1].Should().Be(employee2);
            result[2].Should().Be(employee1);
        }

        /// <summary>
        /// Verify input data is sorted correctly according to score 
        /// and then LastName, FirstName combination
        /// </summary>
        [Test]
        public void ShouldSortDataAccordingToLastName()
        {
            List<IEmployeeResult> list = new List<IEmployeeResult>();
            var employee1 = new Employee
            {
                LastName = "Park",
                FirstName = "Deer",
                Score = 20,
            };
            var employee2 = new Employee
            {
                LastName = "Bon",
                FirstName = "Echo",
                Score = 20,
            };
            var employee3 = new Employee
            {
                LastName = "Paradiso",
                FirstName = "Gran",
                Score = 20,
            };

            list.Add(employee1);
            list.Add(employee2);
            list.Add(employee3);

            var result = _sortStrategy.SortData(list);

            result.Should().NotBeNullOrEmpty();

            //Verify results against expected
            result[0].Should().Be(employee2);
            result[1].Should().Be(employee3);
            result[2].Should().Be(employee1);
        }
    }
}
