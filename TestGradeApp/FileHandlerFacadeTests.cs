﻿using System;
using System.IO;
using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using IO;


namespace TestGradeApp
{
    [TestFixture]
    public class FileHandlerFacadeTests
    {
        FileHandlerFacade _facade;

        [OneTimeSetUp]
        public void SetupFacade()
        {
            _facade = new FileHandlerFacade();
        }

        /// <summary>
        /// Test for exception on an null file name argument for ReadFile
        /// </summary>
        [Test]
        public void ThrowExceptionOnNullFileName()
        {
            Action result = () => _facade.ReadFile("");
            result.ShouldThrow<FileNotFoundException>().And.Message.Should().Contain("File not found.");
        }

        /// <summary>
        /// Test for exception on an Invalid file name argument for ReadFile
        /// </summary>
        [Test]
        public void ThrowExceptionOnInvalidFileName()
        {
            Action result = () => _facade.ReadFile("D:\\NoFileHere.txt");
            result.ShouldThrow<FileNotFoundException>().And.Message.Should().Contain("File not found.");
        }

        /// <summary>
        /// Verify the input file is read correctly
        /// </summary>
        [Test]
        public void VerifyFileReading()
        {   
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var file = string.Format("{0}\\Test\\Input.txt", 
                Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())));
            
            var result = _facade.ReadFile(file);

            result.Should().NotBeNullOrEmpty();
            result[0].FirstName.ShouldBeEquivalentTo("Deer");
            result[0].LastName.ShouldBeEquivalentTo("Park");
            result[0].Score.ShouldBeEquivalentTo("50");
            result[1].LastName.ShouldBeEquivalentTo("Bon");
            result[1].FirstName.ShouldBeEquivalentTo("Echo");
            result[1].Score.ShouldBeEquivalentTo("20");
        }

        /// <summary>
        /// Verify that corrupt data in input file is reported correctly
        /// </summary>
        /// <param name="data"></param>
        [Test]
        [TestCase("12, 3%%")]
        [TestCase("^^^, 7655%, *&XX, FFF")]
        [TestCase("Jack, 12, Box")]
        [TestCase("50, Jack, Box")]
        [TestCase("50, Jack")]
        [TestCase("Jack")]
        [TestCase("50")]
        public void ThrowExceptionForCorruptDataInFile(string data)
        {
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = string.Format("{0}\\InvalidInput.txt", Environment.CurrentDirectory);
            using (var streamWriter = new StreamWriter(filePath))
            {
                streamWriter.WriteLine(data);
                streamWriter.Close();
            }

            Action result = () => _facade.ReadFile(filePath);

            result.ShouldThrow<Exception>()
                .And.Message.Should().Contain("Input file contains invalid data.");

            // Delete the test file
            File.Delete(filePath);
        }

        /// <summary>
        /// Verify that the output file is written to.
        /// </summary>
        [Test]
        public void TestFileWrite()
        {
            var employees = new List<IEmployeeResult>();
            employees.Add(new Employee
            {
                LastName = "Park",
                FirstName = "Deer",
                Score = 50
            });
            employees.Add(new Employee
            {
                LastName = "Bon",
                FirstName = "Echo",
                Score = 20,
            });

            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var file = string.Format("{0}\\output.txt", Environment.CurrentDirectory);
            _facade.WriteFile(employees, file);
            File.Exists(file).Should().BeTrue();
            var readContents = _facade.ReadFile(file);
            readContents.Should().NotBeNullOrEmpty();
            readContents.Count.Should().Be(2);
            readContents[0].FirstName.ShouldBeEquivalentTo("Deer");
            readContents[0].LastName.ShouldBeEquivalentTo("Park");
            readContents[0].Score.ShouldBeEquivalentTo("50");
            readContents[1].LastName.ShouldBeEquivalentTo("Bon");
            readContents[1].FirstName.ShouldBeEquivalentTo("Echo");
            readContents[1].Score.ShouldBeEquivalentTo("20");

            // Remove the temp file
            File.Delete(file);
        }
    }
}
