﻿using System;
using System.IO;

namespace GradeApp
{
    public sealed class CommandLineReader
    {
        /// <summary>
        /// Parse the command line parameters to retrieve the input file name path
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public string ReadFileName(string[] args)
        {
            if (args == null || args.Length == 0)
                throw new ArgumentNullException("Input file not specified.");

            // Parse the command line
            if (!File.Exists(args[0]))
            {
                throw new ArgumentException(string.Format("File {0} does not exist", args[0]));
            }

            return args[0];
        }
    }
}
