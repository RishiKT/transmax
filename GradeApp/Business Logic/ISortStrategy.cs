﻿using IO;
using System.Collections.Generic;

namespace GradeApp
{   
    /// <summary>
    /// ISortStrategy interface
    /// Implement this interface to design your own custom sorting algorithm
    /// </summary>
    public interface ISortStrategy
    {
        IList<IEmployeeResult> SortData(IList<IEmployeeResult> fileData);
    }
}
