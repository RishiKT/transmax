﻿using IO;
using System;
using System.Collections.Generic;

namespace GradeApp
{
    /// <summary>
    /// Context class for the Sort interface
    /// </summary>
    public class SortContext
    {
        ISortStrategy _SortStrategy;

        /// <summary>
        /// Constructor initialises the ISortStrategy interface type.
        /// </summary>
        /// <param name="SortStrategy"></param>
        public SortContext(ISortStrategy SortStrategy)
        {
            if (SortStrategy == null)
            {
                throw new ArgumentNullException(
                    string.Format("Null argument passed: {0}", SortStrategy.ToString()));
            }
            _SortStrategy = SortStrategy;
        }

        /// <summary>
        /// Call the corresponding SortData in the ISortStrategy interface
        /// </summary>
        /// <param name="fileData"></param>
        /// <returns></returns>
        public IList<IEmployeeResult> SortData(IList<IEmployeeResult> fileData)
        {
            return _SortStrategy.SortData(fileData);
        }
    }
}
