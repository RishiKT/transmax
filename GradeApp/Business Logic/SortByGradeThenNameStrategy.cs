﻿using IO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GradeApp
{
    public sealed class SortByGradeThenNameStrategy : ISortStrategy
    {
        /// <summary>
        /// ISortStrategy implementation by Grade, LastName, FirstName in that order.
        /// </summary>
        /// <param name="fileData"></param>
        /// <returns></returns>
        public IList<IEmployeeResult> SortData(IList<IEmployeeResult> fileData)
        {
            if (fileData == null || fileData.Count() == 0)
            {
                throw new ArgumentException("Sort data missing.");
            }

            // Sort by descending grade then by lastname asc and by first name asc
            return fileData.OrderByDescending(i => i.Score)
                                .ThenBy(i => i.LastName, StringComparer.OrdinalIgnoreCase)
                                .ThenBy(i => i.FirstName, StringComparer.OrdinalIgnoreCase).ToList().AsReadOnly();
        }
    }
}
