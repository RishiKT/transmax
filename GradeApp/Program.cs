﻿using IO;
using System;
using System.Collections.Generic;
using System.IO;


namespace GradeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Parse the command line argument
            var cmdLineReader = new CommandLineReader();
            var inputFileName = cmdLineReader.ReadFileName(args);

            //Create the file paths
            var outputFileName = string.Format("{0}-graded.txt", Path.GetFileNameWithoutExtension(inputFileName));
            var outputFileNameWithPath = string.Format("{0}{1}",
                Path.GetDirectoryName(inputFileName), outputFileName);

            // Facade interface for reading file
            FileHandlerFacade facade = new FileHandlerFacade();
            IList<IEmployeeResult> fileResults = facade.ReadFile(inputFileName);

            // Sort the read data
            SortContext context = new SortContext(new SortByGradeThenNameStrategy());
            IList<IEmployeeResult> sortedData = context.SortData(fileResults);

            // Write the sorted data back to output file
            facade.WriteFile(sortedData, outputFileNameWithPath);

            Console.WriteLine("Finished: created names-graded.txt");

        }
    }
}
