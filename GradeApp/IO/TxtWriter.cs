﻿using System.Collections.Generic;

namespace IO
{
    /// <summary>
    /// Write to the Text file
    /// </summary>
   public  class TXTWriter:BaseWriter
    {
    
       public TXTWriter(string FileName):base(FileName)
       {
          
       }
       public TXTWriter()
           : base()
       {
       }

       /// <summary>
       /// Overridden to write to a text file
       /// </summary>
       /// <param name="writeData"></param>
       public override void Write(IList<IEmployeeResult> writeData)
        {
            Delimiter = ",";
            base.Write(writeData);
        }

       
        public override string ToString()
        {
            return FileType.TXT.ToString();
        }
    }
}
