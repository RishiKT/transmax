﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IO
{
    public interface IEmployee
    {
        string LastName { get; set; }
        string FirstName { get; set; }
        int Score { get; set; }
    }

    public interface IEmployeeResult : IEmployee
    {
        List<string> ErrorMessages { get; set; }
    }
    
    public class Employee : IEmployeeResult
    {
        [Required(ErrorMessage = "The Last Name is Empty")]
        public string LastName
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The First Name is Empty")]
        public string FirstName
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The score is Empty  or Invalid")]
        public int Score
        {
            get;
            set;
        }

        public List<string> ErrorMessages
        {
            get;
            set;
        }
    }

}
