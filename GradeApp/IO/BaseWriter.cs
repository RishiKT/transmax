﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IO
{
    public abstract class BaseWriter
    {
        public BaseWriter()
        {

        }
        public BaseWriter(string FileName)
            : this()
        {
            this.FileName = FileName;
        }

        public virtual string Delimiter { get; set; }

        public virtual void Write(IList<IEmployeeResult> writeData)
        {
            string lineString;

            using (FileStream stream = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Write))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    foreach (var data in writeData)
                    {
                        lineString = string.Format("{0}, {1}, {2}", data.LastName, data.FirstName,data.Score);
                        writer.WriteLine(lineString);
                        Console.WriteLine(lineString);
                    }
                }
            }
        }

        public string FileName { get; set; }

    }
}
