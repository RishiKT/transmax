﻿using System.Collections.Generic;
using System.IO;

namespace IO
{
    /// <summary>
    /// The Facade interface for file reading/writing operations.
    /// Hides the implementation details of different types of file readers/writers
    /// </summary>
    public class FileHandlerFacade 
    {
        FileHandlerFactory _factory = null;
        BaseReader _reader = null;
        BaseWriter _writer = null;

        public FileHandlerFacade()
        {
            _factory = new FileHandlerFactory();
           
        }

        /// <summary>
        /// File reader interface
        /// </summary>
        /// <param name="inputFileName"></param>
        /// <returns></returns>
        public IList<IEmployeeResult> ReadFile(string inputFileName)
        {
            ValidateFile.Validate(inputFileName);
            FileType type = GetExtension(inputFileName);
            _reader = _factory.GetReaderObject(type.ToString());
            _reader.FileName = inputFileName;
            return _reader.Read();
        }

        /// <summary>
        /// File writer interface. Can be customized to make reading and writing to
        /// different file types
        /// </summary>
        /// <param name="writeData"></param>
        /// <param name="outputFileName"></param>
        public void WriteFile(IList<IEmployeeResult> writeData, string outputFileName)
        {
            FileType type = GetExtension(outputFileName);
            _writer = _factory.GetWriterObject(type.ToString());
            _writer.FileName = outputFileName;
            _writer.Write(writeData);
        }

        public FileType GetExtension(string FileName)
        {
            string strFileType = Path.GetExtension(FileName).ToLower();
            switch (strFileType)
            {
                case ".txt":
                    return FileType.TXT;
                case ".xls":
                    return FileType.EXCEL;
                case ".xlsx":
                    return FileType.EXCEL;
                default:
                    return FileType.CSV;
            }
        }
    }
}
