﻿using System.Collections.Generic;

namespace IO
{
    /// <summary>
    /// Read the TXT file
    /// </summary>
   public  class TXTReader : BaseReader 
    {
    
       public TXTReader(string FileName):base(FileName)
       {
          
       }
       public TXTReader()
           : base()
       {
       }

       /// <summary>
       /// Overridden to read from a text file
       /// </summary>
       /// <returns></returns>
       public override IList<IEmployeeResult> Read()
        {
            Delimiter = ",";
            return base.Read();
        }

        public override string ToString()
        {
            return FileType.TXT.ToString();
        }
    }
}
