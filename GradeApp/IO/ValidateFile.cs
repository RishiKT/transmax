﻿using System;
using System.IO;

namespace IO
{
    public sealed class ValidateFile 
    {  
        public static void Validate(string FileName)
        {
            if (!File.Exists(FileName))
            {   
                throw new FileNotFoundException("File not found.");
            }
                
        }
    }
}