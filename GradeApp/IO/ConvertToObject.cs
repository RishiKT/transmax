﻿using System;

namespace IO
{
    public class ConvertToObject
    {
        /// <summary>
        /// Converting the string of array into object.
        /// </summary>
        /// <param name="LineString"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static IEmployeeResult GetObject(string LineString, string separator)
        {
            IEmployeeResult Employee;
            string[] stringarray = LineString.Split(separator.ToCharArray());
            try
            {
                Employee = new Employee()
                {
                    LastName = stringarray[0].Trim(),
                    FirstName = stringarray[1].Trim(),
                    Score = Convert.ToInt32(stringarray[2].Trim())
                };
            }
            catch (Exception)
            {
                throw new Exception("Input file contains invalid data.");
            }
            
            return Employee;
        }
       
    }
}
