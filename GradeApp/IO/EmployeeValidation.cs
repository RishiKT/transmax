﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IO
{
    public sealed class EmployeeValidation
    {
        /// <summary>
        /// Validate the read data and fill in error messages for incorrect ones
        /// </summary>
        /// <param name="emp"></param>
        public static void Validate(IEmployeeResult emp)
        {   
            var results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(emp);
            List<string> errors = new List<string>();
            bool IsValid = Validator.TryValidateObject(emp, context, results);
            if (!IsValid)
            {
                foreach (var validationResult in results)
                {
                   errors.Add(validationResult.ErrorMessage);
                }
                emp.ErrorMessages = errors;
            }

        }
    }
}
