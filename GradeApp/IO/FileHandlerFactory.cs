﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace IO
{
    public enum FileType
    {
        TXT,
        CSV,
        EXCEL
    }
    public abstract class GenericFactory<TEntityRead, TEntityWrite>
    {
        public abstract TEntityRead GetReaderObject(string type);
        public abstract TEntityWrite GetWriterObject(string type);
    }

    /// <summary>
    /// 
    /// </summary>
    public class FileHandlerFactory : GenericFactory<BaseReader,BaseWriter>
    {
        Dictionary<string, BaseReader> _readers = new Dictionary<string, BaseReader>();
        Dictionary<string, BaseWriter> _writers = new Dictionary<string, BaseWriter>();

        /// <summary>
        /// Load the assembly dynamically which are inheriting from BaseReader and 
        /// BaseWriter class. We can add any number of classes which inherit from BaseReader
        /// and BaseWriter.
        /// </summary>
        public FileHandlerFactory()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            Type[] types = assembly.GetTypes();
            foreach (Type type in types)
            {
                if (type.IsSubclassOf(typeof(BaseReader)) && (!type.IsAbstract))
                {
                    BaseReader parser = Activator.CreateInstance(type) as BaseReader;
                    _readers.Add(parser.ToString(), parser);
                }
                if (type.IsSubclassOf(typeof(BaseWriter)) && (!type.IsAbstract))
                {
                    BaseWriter writer = Activator.CreateInstance(type) as BaseWriter;
                    _writers.Add(writer.ToString(), writer);
                }
            }
        }

        public override BaseReader GetReaderObject(string type)
        {
            return _readers[type];
        }

        public override BaseWriter GetWriterObject(string type)
        {
            return _writers[type];
        }
    }


}
